//
//  ViewController.m
//  PictureZoom
//
//  Created by QinTuanye on 2018/4/18.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scaleScroll;
@property (weak, nonatomic) IBOutlet UIImageView *scaleImageView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    // 设置代理
    self.scaleScroll.delegate = self;
    
    // 设置缩放比例
    self.scaleScroll.maximumZoomScale = 2;
    self.scaleScroll.minimumZoomScale = 0.5;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 通过实现代理方法告诉scroll缩放哪个控件
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.scaleImageView;
}

@end
