//
//  ViewController.m
//  ScrollViewSimple
//
//  Created by QinTuanye on 2018/4/18.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *imageScroll;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置滚动范围
//    CGSize imageSize = self.imageView.frame.size;
//    self.imageScroll.contentSize = imageSize;
    self.imageScroll.contentSize = CGSizeMake(573, 470);
    // 设置偏移量
//    self.imageScroll.contentOffset = CGPointMake(-100, -100);
    //增加上左下右的额外滚动范围
    self.imageScroll.contentInset = UIEdgeInsetsMake(20, 40, 80, 100);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)scroll:(id)sender {
    // 1. 获取scroll的偏移量
    CGPoint imageOffset = self.imageScroll.contentOffset;
    CGFloat offsetX = imageOffset.x + 10;
    CGFloat offsetY = imageOffset.y + 10;
    // 不带动画滚动
//    self.imageScroll.contentOffset = CGPointMake(offsetX, offsetY);
    CGPoint newImageOffset = CGPointMake(offsetX, offsetY);
    // 带动画滚动
    [self.imageScroll setContentOffset:newImageOffset animated:YES];
}

@end
