//
//  ViewController.m
//  ScrollViewSimple
//
//  Created by QinTuanye on 2018/4/18.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *imageScroll;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置滚动范围
    CGSize imageSize = self.imageView.frame.size;
    self.imageScroll.contentSize = imageSize;
    //self.imageScroll.contentSize = CGSizeMake(573, 470);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
