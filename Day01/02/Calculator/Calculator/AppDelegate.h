//
//  AppDelegate.h
//  Calculator
//
//  Created by QinTuanye on 2018/4/17.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

